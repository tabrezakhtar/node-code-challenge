const sqlite3 = require('sqlite3');
const open = require('sqlite').open;

const connect = async() => {
  let db;
  try {
    db = await open({filename: './data/locations.sqlite', driver: sqlite3.Database});
  } catch (err) {
    console.error('Database connection error', err.message);
  }

  return db;
};

module.exports = {connect};