import "@babel/polyfill";
import React from "react";
import ReactDOM from "react-dom";
import './css/style.css';
import Locations from './locations';

const wrapper = document.getElementById("root");
wrapper ? ReactDOM.render(<Locations />, wrapper) : false;