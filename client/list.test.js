import React from 'react';
import { shallow } from 'enzyme';
import List from './list';

describe('when the list is rendered with no locations passed in', () => {
  it('displays the ui', () => {
    const wrapper = shallow(<List/>);

    expect(wrapper.containsMatchingElement(
      <div className="list"></div>
    )).toEqual(true);
  });
});

describe('when the list is rendered with some results', () => {
  it('displays a list of locations', () => {
    const locations = [
      {geonameid: 1, name: 'location1', latitude: '123', longitude: '456'},
      {geonameid: 2, name: 'location2', latitude: '123', longitude: '456'},
      {geonameid: 3, name: 'location3', latitude: '123', longitude: '456'}
    ];
    const wrapper = shallow(<List locations={locations}/>);

    expect(wrapper.containsMatchingElement(
      <div className="list">
        <div className="row">
          <div className="name header">Name</div>
          <div className="latitude header">Latitude</div>
          <div className="longitude header">Longitude</div>
        </div>
        <div className="row" key={1}>
          <div className="name">location1</div>
          <div className="latitude">123</div>
          <div className="longitude">456</div>
        </div>
        <div className="row" key={2}>
          <div className="name">location2</div>
          <div className="latitude">123</div>
          <div className="longitude">456</div>
        </div>
        <div className="row" key={3}>
          <div className="name">location3</div>
          <div className="latitude">123</div>
          <div className="longitude">456</div>
        </div>
      </div>
    )).toEqual(true);
  });
});