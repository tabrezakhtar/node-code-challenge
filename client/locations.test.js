import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import * as axios from "axios";
import Locations from './locations';
import List from './list';

jest.mock('axios');

afterEach( () => {
  jest.restoreAllMocks();
});

describe('when the component loads', () => {
  it('displays the ui', () => {
    const wrapper = shallow(<Locations />);

    expect(wrapper.containsMatchingElement(
      <div className="container">
        <input type="text" placeholder="Please enter location.."/>
        <List />
      </div>
    )).toEqual(true);
  });
});

describe('when the locations input is searched with less than 2 characters', () => {
  it('does not make the ajax call', () => {
    const response = {
      data: {
        results: []
      }
    };

    axios.get.mockResolvedValue(response);
    const wrapper = shallow(<Locations />);
    wrapper.find('#location').simulate('change', { target: { value: 'L' } });
    expect(axios.get).not.toHaveBeenCalledWith('/locations?q=L');
  });
});

describe('when the locations input is searched with 2 or more characters', () => {
  it('makes the ajax call', () => {
    const response = {
      data: {
        results: []
      }
    };
    axios.get.mockResolvedValue(response);
    const wrapper = shallow(<Locations />);
    wrapper.find('#location').simulate('change', { target: { value: 'Le' } });
    expect(axios.get).toHaveBeenCalledWith('/locations?q=Le');
  });
});