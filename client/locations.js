import React, { useState } from 'react';
import axios from 'axios';
import List from './list';

const Locations = () => {

  const [locations, setLocations] = useState([]);

const handleChange = async(e) => {
  const query = e.target.value;
    if (query.length >= 2) {
      const response = await fetch(`/locations?q=${query}`);
      const json = await response.json();
      setLocations(json.results);
    } else {
      setLocations([]);
    }
}

  return <div className="container">
    <input id="location" type="text" placeholder="Please enter location.." onChange={handleChange}/>
    <List locations={locations} />
  </div>
};

export default Locations;