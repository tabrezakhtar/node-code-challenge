import React, { useState } from 'react';

const List = ({locations}) => {
  if (!locations || !locations.length) {
    return <div className="list"></div>
  }

  return <div className="list">
    <div className="row">
      <div className="name header">Name</div>
      <div className="latitude header">Latitude</div>
      <div className="longitude header">Longitude</div>
    </div>
    {locations.map(l => <div className="row" key={l.geonameid}>
      <div className="name">{l.name}</div>
      <div className="latitude">{l.latitude}</div>
      <div className="longitude">{l.longitude}</div>
    </div>)}
  </div>
};

export default List;