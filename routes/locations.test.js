const httpMocks = require('node-mocks-http');

function setupEndpoint(error) {
  let mockDb = null;

  if (error) {
    mockDb = { all: jest.fn().mockRejectedValue(new Error('oops')) }
  } else {
    mockDb = { all: jest.fn().mockResolvedValue({"some": "locations"}) };
  }

  const { queryLocations } = require('./locations')(mockDb);
  return queryLocations;
}

beforeEach(() => {
  jest.resetModules();
});

afterEach( () => {
  jest.restoreAllMocks();
});


it('errors', async (done) => {
  const queryLocations = setupEndpoint();

  const httpRequest  = httpMocks.createRequest({
    query: {
      q: null
    }
  });

  const httpResponse = httpMocks.createResponse();
  const response = await queryLocations(httpRequest, httpResponse);
  const result = response._getJSONData();
  expect(response.statusCode).toBe(400);
  expect(result.message).toBe('Query must be greater than 1 character.');
  done();
});

describe('when there is no query', () => {
  it('returns an error', async (done) => {
    const queryLocations = setupEndpoint();
    const httpRequest  = httpMocks.createRequest({
      query: {
        q: null
      }
    });

    const httpResponse = httpMocks.createResponse();
    const response = await queryLocations(httpRequest, httpResponse);
    const result = response._getJSONData();
    expect(response.statusCode).toBe(400);
    expect(result.message).toBe('Query must be greater than 1 character.');
    done();
  });
});

describe('when there is 1 character', () => {
  it('returns an error', async (done) => {
    const queryLocations = setupEndpoint();
    const httpRequest  = httpMocks.createRequest({
      query: {
        q: 'l'
      }
    });

    const httpResponse = httpMocks.createResponse();
    const response = await queryLocations(httpRequest, httpResponse);
    const result = response._getJSONData();
    expect(response.statusCode).toBe(400);
    expect(result.message).toBe('Query must be greater than 1 character.');
    done();
  });
});

describe('when there is valid query of 2 characters or more', () => {
  it('returns some locations', async (done) => {
    const queryLocations = setupEndpoint();
    const httpRequest = httpMocks.createRequest({
      query: {
        q: 'leeds'
      }
    });

    const httpResponse = httpMocks.createResponse();
    const response = await queryLocations(httpRequest, httpResponse);
    const result = response._getData();
    expect(response.statusCode).toBe(200);
    expect(result.results).toEqual({some: 'locations'});
    done();
  });
});

describe('when there is server error', () => {
  it('returns an error', async (done) => {
    const queryLocations = setupEndpoint(true);
    const httpRequest = httpMocks.createRequest({
      query: {
        q: 'leeds'
      }
    });

    const httpResponse = httpMocks.createResponse();
    const response = await queryLocations(httpRequest, httpResponse);
    const result = response._getJSONData();
    expect(response.statusCode).toBe(500);
    expect(result.message).toEqual('oops');
    done();
  });
});