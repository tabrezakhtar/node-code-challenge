const express = require('express');
const router = express.Router();

module.exports = function (db) {
  const locationsRouter = require('./locations')(db);
  
  router.get('/', locationsRouter.queryLocations);
  router.get('/test', locationsRouter.test);

  return router;
};