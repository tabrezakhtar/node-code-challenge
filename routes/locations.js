module.exports = function (db) {
  async function queryLocations(req, res) {
    try {
      const query = req.query.q;
  
      if (!query || query.length < 2) {
        console.log('here');
        return res.status(400).json({ message: 'Query must be greater than 1 character.' });
      }
  
      const results = await db.all('SELECT geonameid, name, latitude, longitude FROM locations WHERE name LIKE ?', `%${query}%`);
      debugger;
      return res.status(200).send({results});
    } catch(ex) {
      return res.status(500).json({ message: ex.message })
    }
  }

  async function test(req, res) {
    return res.status(200).send({test: 123});
  }

  return {queryLocations, test}
};
