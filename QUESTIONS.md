# Questions

Qs1: Explain the output of the following code and why

```js
    setTimeout(function() {
      console.log("1");
    }, 100);
    console.log("2");
```
Answer:
```
Output:
"2"
"1"
```
The setTimeout has a delay of 100 milliseconds so will evaluate the function after `console.log("2")`.

Qs2: Explain the output of the following code and why

```js
    function foo(d) {
      if(d < 10) {
        foo(d+1);
      }
      console.log(d);
    }
    foo(0);
```
Answer:
```
Output:
10
9
8
7
6
5
4
3
2
1
0
```
`foo()` is a recursive function. Calling `foo()` with an inital value will call itself again with the initial value+1.  This pushes the call onto the stack until the value is 10, where it will start to unwind the calls and execute the rest of the function.  The call stack is popped so this will result in the numbers logged from 10 to 0.


Qs3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
    function foo(d) {
      d = d || 5;
      console.log(d);
    }
```
Answer:

Mutating variables can cause unintended side effects because objects are passed by reference in JavaScript.  It is better practice to create a new object, and leave the original as is.

Qs4: Explain the output of the following code and why

```js
    function foo(a) {
      return function(b) {
        return a + b;
      }
    }
    var bar = foo(1);
    console.log(bar(2))
```
Answer:
```
Output:
3
```
`foo(1)` returns a closure which is assigned to `bar`.  When `bar(2)` is called, it still has access to the value of param `a` since closures capture the state of a call, and adds it to `b` resulting in the output of 3.


Qs5: Explain how the following function would be used

```js
    function double(a, done) {
      setTimeout(function() {
        done(a * 2);
      }, 100);
    }
```
Answer:  
The double function accepts a callback of `done`.  This is passed in because the double function contains an asynchronous function which calls `done` when the operation is completed.  This pattern can be used to signal the end of the async operation e.g. In unit tests when testing an async functions.